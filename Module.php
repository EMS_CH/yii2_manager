<?php
/**
 * Created by PhpStorm.
 * User: ffeni
 * Date: 02.02.2019
 * Time: 22:04
 */

namespace fenix957\MenuManage ;

use Yii;
use yii\base\Module as BaseModule;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;

class Module extends BaseModule
{
    public function init()
    {

        $x = new MainBootstrapClass();
        parent::init();


    }

    public static function GetAllModulesActions(){
        $controllers = FileHelper::findFiles(Yii::getAlias('@app/modules'), ['recursive' => true]);
        $result = [];
        foreach ($controllers as $controller) {


            if (substr($controller, strlen($controller) - strlen("Controller.php")) == "Controller.php") {

                $contents = file_get_contents($controller);
                $controllerId = Inflector::camel2id(substr(basename($controller), 0, -14));

                preg_match_all("/namespace app\\\modules\\\EMS\\\(\w+?)\\\controllers;/", $contents, $name_cnt);
                $cnt_name = 'Undefined';
                if(count($name_cnt[1])>0){
                    $cnt_name =       trim(Inflector::camel2id($name_cnt[1][0])) ;


                }


                if(!isset($result[$cnt_name])) {
                    $result[$cnt_name] = [];
                }



                preg_match_all('/Action for (.*?)\\((.*?)\\)([\w \W\s \G ]{1,}?)public function action(.*?)\\(/im', $contents, $result_d,PREG_SET_ORDER);

                foreach ($result_d as $action) {

                    $f_action_id = trim(Inflector::camel2id($action[1]));
                    $s_action_id = trim(Inflector::camel2id($action[4]));
                    $title = $action[2];

                    if(!isset($result[$cnt_name][$controllerId][$f_action_id])){

                        if(trim($f_action_id) == trim($s_action_id)){
                            $result[$cnt_name][$controllerId][$f_action_id]     = $title;
                        }

                    }

                }

            }


        }

        return $result;
    }

}