<?php
namespace fenix957\MenuManage\controllers ;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class DefaultController extends Controller
{


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {


        if(\Yii::$app->user->can('SuperUser')){
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [

                        [
                            'actions' => [ 'index'],
                            'allow' => true,

                        ],
                    ],
                ],

            ];


        } else {


            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'roles' => ['@','?'],
                            'allow' => false,
                        ],

                    ],
                ],

            ];

        }

    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


}
