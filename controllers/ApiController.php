<?php
namespace fenix957\MenuManage\controllers ;

use fenix957\MenuManage\models\EmsConfig;
use fenix957\MenuManage\models\EmsMenu;
use fenix957\MenuManage\models\EmsMenuAc;
use fenix957\MenuManage\models\EmsRules;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class ApiController extends Controller
{


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {


        if(\Yii::$app->user->can('SuperUser')){
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [

                        [

                            'allow' => true,

                        ],
                    ],
                ],

            ];


        } else {


            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [

                            'roles' => ['@','?'],
                            'allow' => false,
                        ],

                    ],
                ],

            ];

        }

    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return ;
    }

    public function actionChangeModulePerm($module_id,$controller_id,$action_id,$role_id,$enabled){

        $model = EmsRules::findOne([
            'module_id'=>$module_id,
            'controller_id'=>$controller_id,
            'action_id'=>$action_id,
            'role'=>$role_id

            ]);
        if(!$model){
            throw new NotFoundHttpException('Правило не найдено',404);
        }

        $enabled = (int)$enabled;
        if($enabled ==0 || $enabled ==1){
            $model->enabled =  $enabled;
            $model->save();
            return true;
        } else {
            throw new BadRequestHttpException('Неправильный статус для роли',400);
        }



    }


    public function actionNewMenuItem(){



        $model = new EmsMenu();
        $parents = EmsMenu::find()->where(['parent'=>null])->all();
        $model = new EmsMenu();
        $aj = Yii::$app->request ;
        if (isset($aj->bodyParams['ajax'])&& $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {


            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => $model->save(),'id'=>$model->id];

        }


        return $this->renderAjax('new-menu',['model'=>$model,'parents'=>$parents]);


    }
    public function actionEditMenuItem($id){



        $model =  EmsMenu::findOne(['id'=>$id]);
        if(!$model){
            throw new BadRequestHttpException('Пункт не найден',400);
        }
        $parents = EmsMenu::find()->where(['parent'=>null])->all();

        $aj = Yii::$app->request ;
        if (isset($aj->bodyParams['ajax'])&& $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {


            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => $model->save(),'id'=>$model->id];

        }


        return $this->renderAjax('edit-menu',['model'=>$model,'parents'=>$parents]);


    }
    public function actionGetMenuTree(){

        $menu = EmsMenu::find()->orderBy('order')->all();

       return  $this->renderAjax('menu-tree',['menu'=>$menu]);
    }
    public function actionMenuTreeUpdate($data){

            $max_item = EmsMenu::find()->orderBy(['order'=>SORT_DESC])->one();

            $data = json_decode($data,true);
            $data= $data[0];
            var_dump($data);
            $pos = $max_item->order +1;
            foreach ($data as $items){
                if(!isset($items['id'])){
                    continue;
                }
                $parent_menu = EmsMenu::findOne(['id'=>$items['id']]);
                if($parent_menu){
                    $parent_menu->parent = null;
                    $parent_menu->order = $pos++ ;
                    $parent_menu->save();

                }

                if(isset($items['children'])){
                    foreach ($items['children'][0] as $ch_data){
                        if(isset($ch_data['id'])){
                            $menu_item = EmsMenu::findOne(['id'=>$ch_data['id']]);


                            if($menu_item){

                                $menu_item->parent = $items['id'];
                                $menu_item->order = $pos++ ;
                                $menu_item->save();


                            }
                        }



                    }
                }



            }



            return '';
        }
    public function actionGetMenuEditOne($id){



        $model =  EmsMenu::findOne(['id'=>$id]);
        if(!$model){
            throw new BadRequestHttpException('Пункт не найден',400);
        }
        $parents = EmsMenu::find()->where(['parent'=>null])->all();

        $aj = Yii::$app->request ;
        if (isset($aj->bodyParams['ajax'])&& $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {


            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => $model->save(),'id'=>$model->id];

        }


        return $this->renderAjax('menu-edit-one',['model'=>$model,'parents'=>$parents]);


    }

    public function actionUpRule($menu_id,$role_name,$visible){

        $model = EmsMenuAc::findOne(['menu_id'=>$menu_id,'role_name'=>$role_name]);


        if($model){
            $model->visible = $visible;
            $model->save();
            return 'saved';
        }

    }


    public function actionEmsConfigUpdate(){


        if (Yii::$app->request->post()) {
            $data = Yii::$app->request->post();
            $value = $data['value'];
            $pk = $data['pk'];
            $name = $data['name'];



        $item = EmsConfig::findOne(['id'=>$pk]);

        if($item){
            $item->param_value =$value;
            $item->save();
            return 1;
        } else {
            return 0;
        }

        }
        return 0;
    }


}
