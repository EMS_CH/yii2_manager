<?php

namespace fenix957\MenuManage\models;

use Yii;

/**
 * This is the model class for table "ems_config".
 *
 * @property int $id
 * @property string $param_name Наименование параметра
 * @property string $param_value Данные
 * @property string $category Категория
 */
class EmsConfig extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ems_config';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['param_name', 'param_value', 'category'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'param_name' => 'Наименование параметра',
            'param_value' => 'Данные',
            'category' => 'Категория',
        ];
    }
}
