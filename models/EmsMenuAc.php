<?php

namespace fenix957\MenuManage\models;

use Yii;
use fenix957\MenuManage\models\EmsMenu as Menu;
/**
 * This is the model class for table "menu_ac".
 *
 * @property int $id
 * @property int $menu_id
 * @property string $role_name
 * @property int $visible
 *
 * @property Menu $menu
 */
class EmsMenuAc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu_ac';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menu_id', 'visible'], 'integer'],
            [['role_name'], 'string', 'max' => 255],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'role_name' => 'Role Name',
            'visible' => 'Visible',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }
}
