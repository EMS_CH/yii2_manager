<?php

namespace fenix957\MenuManage\models;

use Yii;

/**
 * This is the model class for table "ems_rules".
 *
 * @property int $id
 * @property string $module_id
 * @property string $controller_id
 * @property string $action_id
 * @property string $role
 * @property int $enabled
 * @property int $description
 */
class EmsRules extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ems_rules';
    }

    public static function get_assoc(){

        $EMS_DB_DATA_bin=   EmsRules::find()->all();
        foreach ($EMS_DB_DATA_bin as $e){

            $EMS_DB_DATA[$e->module_id][$e->controller_id][$e->action_id][$e->role] = $e->enabled;


        }
        return $EMS_DB_DATA;

    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['enabled'], 'integer'],
            [['module_id', 'controller_id', 'action_id', 'role','description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'module_id' => 'Module ID',
            'controller_id' => 'Controller ID',
            'action_id' => 'Action ID',
            'role' => 'Role',
            'enabled' => 'Enabled',
            'description' => 'description',
        ];
    }
}
