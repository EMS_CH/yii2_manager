<?php
/**
 * Created by PhpStorm.
 * User: ffeni
 * Date: 02.02.2019
 * Time: 21:27
 */

$this->title = "Настройки параметров EMS";

use fenix957\MenuManage\models\EmsRules;
use fenix957\MenuManage\Module as MM;

    $EMS_DATA = MM::GetAllModulesActions();
    $EMS_DB_DATA = EmsRules::get_assoc();
    $roles = Yii::$app->authManager->getRoles();



?>


<div class="row col-md-12">
    <div class="col-12">
        <div class="box">
            <div class="box-header">
                <ul class="nav nav-pills margin-bottom">
                    <li class="nav-item"> <a href="#navpills-1" class="nav-link active show" data-toggle="tab" aria-expanded="false">Основные настройки</a> </li>
                    <li class="nav-item"> <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">Меню</a> </li>
                    <li class="nav-item"> <a href="#navpills-3" class="nav-link" data-toggle="tab" aria-expanded="true">Права</a> </li>

                </ul>
            </div>
            <div class="box-body">
                <div class="tab-content">
                    <div id="navpills-1" class="tab-pane active show">
                        <!-- Categroy 1 -->
                        <div class=" tab-pane animation-fade active" id="category-1" role="tabpanel">
                            <h2>Основные параметры приложения</h2>
                            <?php

                          $items =      \fenix957\MenuManage\models\EmsConfig::find()->all();




                          foreach ($items as $item){

                              ?>
                              <div class="row">
                                  <div class="col-md-1"><?=$item->category   ?></div>
                                  <div class="col-md-2"><?=$item->param_name   ?></div>
                                  <div class="col-md-9"> <a href="#" id="<?=$item->param_name ?>" class="ems_config_items" data-type="text" data-pk="<?=$item->id   ?>" data-url="/ems-conf/api/ems-config-update" data-title="<?=$item->param_name   ?>"><?=$item->param_value   ?></a>
                                  </div>
                              </div>



                              <br>


                              <?php
                          }



                          $this->registerJs("$(document).ready(function() {
    $('.ems_config_items').editable();
});");

                            ?>



                        </div>
                        <!-- End Categroy 1 -->
                    </div>
                    <div id="navpills-2" class="tab-pane">
                        <!-- Categroy 2 -->
                        <div class="tab-pane animation-fade row col-md-12" id="category-2" role="tabpanel">
                            <div class="box box-solid  box-success">
                                <div class="box-header with-border">
                                    <h4 class="box-title">Управление меню </h4>
                                    <a class="add-menu-item btn btn-app btn-success right-float" href="#">
                                        <i class="fa fa-plus"></i> Добавить пункт
                                    </a>
                                </div>

                                <div class="box-body main_menu_data" style="">

                                    <div class="col-md-12 row">
                                        <div class="col-md-6 box box-default">
                                            <div class="box-header with-border">
                                                <h4 class="box-title text-dark">Структура меню</h4>
                                                <h6 class="box-subtitle text-dark">Управление структурой главного меню</h6>


                                            </div>


                                            <div class="menu-main-tree box-body">

                                            </div>

                                        </div>

                                        <div class="col-md-6 box box-default">
                                            <div class="box-header with-border">
                                                <h4 class="box-title text-dark">Пункт меню</h4>
                                                <h6 class="box-subtitle text-dark">Редактирвание пункта меню</h6>


                                            </div>

                                            <div class="menu-main-edit box-body">

                                            </div>

                                        </div>

                                    </div>


                                </div>
                            </div>



                            </div>
                        <!-- End Categroy 2 -->
                    </div>
                    <div id="navpills-3" class="tab-pane">
                        <!-- Управление правами доступа к контроллерам 3 -->
                        <div class="tab-pane animation-fade row col-md-12" id="category-3" role="tabpanel">

                            <?php

                            foreach ($EMS_DATA as $key =>$ed){
                                            ?>

                                <div class="col-md-6 ">
                                    <div class="box box-solid box-inverse box-success">
                                        <div class="box-header with-border">
                                            <h4 class="box-title"><strong>Модуль :</strong> <?= $key ?></h4>

                                        </div>

                                        <div class="box-body" style="">
                                            <?php

                                            foreach ($ed as $cname=>$controller){
                                               ?>
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-striped">
                                                            <caption>Контроллер:<?= $cname ?></caption>
                                                            <thead>
                                                            <tr>
                                                                <td>Action</td>
                                                                <td>Описание</td>
                                                                <td>Разрешения</td>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            foreach ($controller as $cntr=>$ctr_tit){
                                                                ?>
                                                                <tr>
                                                                    <td><?= $cntr ?></td>
                                                                    <td><?= $ctr_tit ?></td>

                                                                    <td>
                                                                        <ul class="list-group perm">
                                                                        <?php

                                                                        foreach ($roles as $role){
                                                                                if($role->name == "SuperUser"){
                                                                                    continue;
                                                                                }
                                                                            ?>
                                                                            <li class="list-group-item">
                                                                             <?= $role->name.' - '.$role->description ?>
                                                                                <div class="material-switch pull-right">
                                                                                    <input id="role<?= $key.$cname.$cntr.$role->name ?>" class="role_change filled-in"
                                                                                           data-module="<?= $key ?>"
                                                                                           data-controller="<?= $cname ?>"
                                                                                           data-action="<?= $cntr ?>" name="<?= $role->name  ?>"

                                                                                           <?php
                                                                                           if(!(isset($EMS_DB_DATA[$key][$cname][$cntr][$role->name ]))){

                                                                                               $perm = new EmsRules();
                                                                                               $perm->description = $ctr_tit ;
                                                                                               $perm->role = $role->name ;
                                                                                               $perm->enabled = 0;
                                                                                               $perm->action_id = $cntr;
                                                                                               $perm->controller_id = $cname;
                                                                                               $perm->module_id = $key;
                                                                                               $perm->save();

                                                                                           } else {
                                                                                           if($EMS_DB_DATA[$key][$cname][$cntr][$role->name ] ==1){
                                                                                               echo " checked ";
                                                                                           }
                                                                                           }
                                                                                           ?>


                                                                                           type="checkbox"/>
                                                                                    <label for="role<?= $key.$cname.$cntr.$role->name?>" class="label-default"></label>
                                                                                </div>
                                                                            </li>
                                                                        <?php


                                                                        }


                                                                        ?>
                                                                        </ul></td>
                                                                </tr>

                                                            <?php
                                                            }
                                                            ?>
                                                            </tbody>

                                                        </table>

                                                    </div>

                                                <?php
                                            }


                                            ?>

                                        </div>
                                    </div>
                                </div>

                            <?php



                            }


                            ?>




                        </div>
                        <!-- End Categroy 3 -->
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>


<?php


$perm_module_change_js = /** @lang JavaScript */
    "

$('.role_change').on('change',function(){
    
    var module = $(this).data('module') ;
    var controller = $(this).data('controller') ;
    var action = $(this).data('action') ;
    var role = $(this).attr('name');
    var ch = 0;
    if(this.checked){
       ch =1; 
    }
    $.ajax({
  type: \"GET\",
  url: \"/ems-conf/api/change-module-perm?module_id=\"+module+'&controller_id='+controller+'&action_id='+action+'&role_id='+role+'&enabled='+ch,
  data: \"name=John&location=Boston\",
  success: function(msg){
     $.toast({
    heading: 'Обновлено',
    position:'top-right',
    text: 'Разрешения были обновлены',
    showHideTransition: 'slide',
    icon: 'success'
}) ;
  } ,
  error:function() {
       $.toast({
    heading: 'Ошибка',
        position:'top-right',
    text: 'Что-то пошло не так, произошла ошибка при сохранении',
    showHideTransition: 'fade',
    icon: 'error'
})
  }
});
    
    
 
    
    
    
 
    
 
    
    
});
";
$this->registerJs($perm_module_change_js);


$menu_js = /** @lang JavaScript */
    "
$(document).on('click','.add-menu-item',function() {

    
 window.new_menu =   $.confirm({
    title: 'Добавить пункт меню',
    content: 'url:/ems-conf/api/new-menu-item',
     columnClass: 'col-md-12',
     buttons:{
          ok: {
            text: 'Закрыть', // With spaces and symbols
            action: function () {
             
            }
        }
     }
});
    
    
    
})


";
$this->registerJs($menu_js);


$menu_tree = /** @lang JavaScript */
    "

$(document).ready(function() {
  $('.menu-main-tree').load('/ems-conf/api/get-menu-tree');
});


$(document).on('click','.item-edit',function() {
  var id = $(this).data('id');
    $('.menu-main-edit').load('/ems-conf/api/get-menu-edit-one?id='+id);

})

";

$this->registerJs($menu_tree);