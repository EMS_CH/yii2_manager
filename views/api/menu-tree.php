<?php


use fenix957\MenuManage\models\EmsMenu;

/**
 * @property EmsMenu[] $menu
 */


$rand_id = rand(5555,666666666);

$main_menu = [];
$menu_parents =[];

foreach ($menu as $m){
    if(is_null($m->parent) ){
        $main_menu[] = $m;
    } else {
        $menu_parents[$m->parent][] = $m;
    }
}


?>

<ul class="menu ui-sortable" id="menu-to-edit<?= $rand_id ?>">

    <?php
    foreach ($main_menu as $mm){
        ?>
    <li data-order="<?= $mm->order ?>" data-id="<?= $mm->id ?>" id="menu-item-<?= $mm->id ?>" class="menu-item menu-item-depth-0 menu-item-custom " style="position: relative; left: 0px; top: 0px;">
        <div class="menu-item-bar">
            <div class="menu-item-handle ui-sortable-handle">
                <span class="item-title"><i class="fa <?= $mm->icon ?>"></i> <span class="menu-item-title"><?= $mm->name ?></span> </span>
                <span class="item-controls">

							<a class="item-edit" data-id="<?= $mm->id ?>""><span class="screen-reader-text">Редактировать</span></a>
						</span>
            </div>

        </div>
        <ul class="">
        <?php
        if(isset($menu_parents[$mm->id])){
            ?>


                <?php
                foreach ($menu_parents[$mm->id] as $mc){
                    ?>


                <li data-order="<?= $mc->order ?>" data-id="<?= $mc->id ?>" id="menu-item-<?= $mc->id ?>" class="menu-item menu-item-depth-0 menu-item-custom " style="position: relative; left: 0px; top: 0px;">
                    <div class="menu-item-bar">
                        <div class="menu-item-handle ui-sortable-handle">
                            <span class="item-title"><i class="fa <?= $mc->icon ?>"></i> <span class="menu-item-title"><?= $mc->name ?></span> </span>
                            <span class="item-controls">

							<a class="item-edit" data-id="<?= $mc->id ?>""><span class="screen-reader-text">Редактировать</span></a>
						</span>

                        </div>
                    </div>

                <?php

                }
                ?>



                </li>




          <?php

        }

        ?>
        </ul>

    </li>







    <?php
    }
    ?>





</ul>



<?php

$menu_tree_js = /** @lang JavaScript */
    "



var oldContainer;
 var item = $(\"#menu-to-edit$rand_id\").sortable({
  placeholder: '<div style=\"   width:382px;   height: 60px;    border: 2px dashed;    color: blue;\"></div>',
  group: 'serialization',
  delay: 500,

  isValidTarget: function (\$item, container) {
        var depth = 1, // Start with a depth of one (the element itself)
            maxDepth = 2,
            children = \$item.find('ul').first().find('li');

        // Add the amount of parents to the depth
        depth += container.el.parents('ul').length;

        // Increment the depth for each time a child
        while (children.length) {
            depth++;
            children = children.find('ul').first().find('li');
        }

        return depth <= maxDepth;
    },

  afterMove: function (placeholder, container) {
    
 
    
    if(oldContainer != container){
      if(oldContainer)
        oldContainer.el.removeClass(\"active\");
      container.el.addClass(\"active\");

      oldContainer = container;
    }
  },
  onDrop: function (\$item, container, _super) {
    
    
    
     var data =  $(\"ul.menu\").sortable('serialize').get();

    var jsonString = JSON.stringify(data, null, '');

     
           $.ajax({
  url: \"/ems-conf/api/menu-tree-update?data=\"+jsonString,
  success: function(data){
      
    
      
   $.toast({
       position: 'top-right',
         icon: 'success',
    title: 'Обновлено',
    text: 'Структура меню обновлена' ,
    type: 'success',
    styling: 'bootstrap3'
});
  }
});
     
     

    container.el.removeClass(\"active\");
    _super(\$item, container);
  }
});

";

        $this->registerJs($menu_tree_js);


