<?php
/**
 * Created by PhpStorm.
 * User: ffeni
 * Date: 03.02.2019
 * Time: 6:33
 */
use  fenix957\MenuManage\models\EmsMenu;
use fenix957\MenuManage\models\EmsMenuAc;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\web\View;
use phpnt\ICheck\ICheck;

/**
 * @property  EmsMenu $model
 * @property  EmsMenu[] $parents
 */

$random_id = rand(1,100000);
$s_parents = [];


 $roles = \Yii::$app->authManager->getRoles();

 $menu_rx = EmsMenuAc::findAll(['menu_id'=>$model->id]);
$menu_rl = [];
 foreach ($menu_rx as $menu_rx_x){
     $menu_rl[$menu_rx_x->role_name] = $menu_rx_x->visible;
 }



?>

<div class="row col-md-12"> <?php
foreach ($parents as $p){
    $s_parents[$p->id] = $p->name ;
}

$format_icon = <<< SCRIPT
function format_icon$random_id(state) {
    if (!state.id) return state.text; // optgroup
  
    return '<i style="font-size: 24px;" class="fa ' + state.id + '"></i>   -' + state.id;
}


$('#new-menu-item$random_id').on('beforeSubmit', function () {
    var \$yiiform = $(this);
    // отправляем данные на сервер
    $.ajax({
            type: \$yiiform.attr('method'),
            url: \$yiiform.attr('action'),
            data: \$yiiform.serializeArray()
        }
    )
    .done(function(data) {
       if(data.success) {
         $('.menu-main-tree').load('/ems-conf/api/get-menu-tree');
        alert('Пункт успешно обновлен');
        } else {
          $('.menu-main-tree').load('/ems-conf/api/get-menu-tree');
          alert('Ошибка при обновлении данных');
        }
    })
    .fail(function () {
      $('.menu-main-tree').load('/ems-conf/api/get-menu-tree');
          alert('Ошибка при добавлении данных');
    })

    return false; // отменяем отправку данных формы
})

SCRIPT;



$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format_icon, View::POS_HEAD);


$icons__b = Yii::$app->params['fa-icons'];
$icons = [];
foreach ($icons__b as $ii){
    $icons[$ii] =$ii;
}
$form = ActiveForm::begin([
    'id' => 'new-menu-item'.$random_id,
    //   'action' => 'save-url',
    'enableAjaxValidation' => true,

    'options' => ['class' => 'col-md-12 form-horizontal form-element'],
    'fieldConfig' => [
        'template' => "<div class='form-group row'>{label}\n<div class='col-sm-9'> {beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}</div></div>",
        'labelOptions'=>['class' => 'col-sm-3 control-label'],
    ],

    //  'validationUrl' => 'my-validation-url',
]); ?>
    <div class="box-body">
        <?= $form->field($model, 'name')->textInput(); ?>
        <?= $form->field($model, 'route')->textInput(); ?>

        <?=       $form->field($model, 'icon')->widget(Select2::classname(), [
            'data' => $icons,
            'options' => ['placeholder' => 'Выберите иконку'],
            'pluginOptions' => [
                'templateResult' => new JsExpression('format_icon'.$random_id),
                'templateSelection' => new JsExpression('format_icon'.$random_id),
                'escapeMarkup' => $escape,
                'allowClear' => true
            ],
        ]); ?>


        <?= Html::submitButton('Обновить элемент',[    'class' => 'btn btn-success btn-lg']); ?>
    </div>
<?php $form->end(); ?>

</div>
<div class="row col-md-12 box box-default">
    <div class="box-header with-border">
        <h4 class="box-title text-dark">Права доступа </h4>
        <h6 class="box-subtitle text-dark">Редактирование прав доступа к выбранному пункту</h6>


    </div>
    <div class="col-md-12 box-body">
<div class="row">
        <?php

        foreach ($roles as $role){
            if(!isset($menu_rl[$role->name])){
                $mr = new EmsMenuAc();
                $mr->role_name = $role->name;
                $mr->visible = 0;
                $mr->menu_id = $model->id;
                $mr->save();
                $menu_rl[$role->name] = 0;
            }



            ?>
<div class="col-md-6">

    <input data-id="<?=  $model->id ?>"   data-role="<?= $role->name ?>" class="menu_role_change<?= $random_id ?>"  type="checkbox" id="<?= $role->name.$random_id ?>"  <?php
    if($menu_rl[$role->name] == 1){
        echo  " checked='checked' ";
    }
    ?>>
    <label for="<?= $role->name.$random_id ?>"><?= $role->name ." - ".$role->description ?></label>


</div>


        <?php




        }

        ?>


</div>
    </div>


</div>



<?php

$js = /** @lang JavaScript */
    "
        $('.menu_role_change$random_id').on('change',function(event) {
         var menu_id = $(this).data('id');
         var role = $(this).data('role');
         var visible = 0;
           if (event.target.checked) {
   visible = 1;
  } 
             $.ajax({
  url: \"/ems-conf/api/up-rule?menu_id=\"+menu_id+'&role_name='+role+'&visible='+visible,
  success: function(data){
     $.toast({
    heading: 'Обновлено',
    position:'top-right',
    text: 'Разрешения были обновлены',
    showHideTransition: 'slide',
    icon: 'success'
}) ;
  }
});
         
        });

";

    $this->registerJs($js);

