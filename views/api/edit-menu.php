<?php
/**
 * Created by PhpStorm.
 * User: ffeni
 * Date: 03.02.2019
 * Time: 6:33
 */
use  fenix957\MenuManage\models\EmsMenu;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\web\View;

/**
 * @property  EmsMenu $model
 * @property  EmsMenu[] $parents
 */

$random_id = rand(1,100000);
$s_parents = [];

foreach ($parents as $p){
    $s_parents[$p->id] = $p->name ;
}

$format_icon = <<< SCRIPT
function format_icon$random_id(state) {
    if (!state.id) return state.text; // optgroup
  
    return '<i style="font-size: 24px;" class="fa ' + state.id + '"></i>   -' + state.id;
}


$('#new-menu-item$random_id').on('beforeSubmit', function () {
    var \$yiiform = $(this);
    // отправляем данные на сервер
    $.ajax({
            type: \$yiiform.attr('method'),
            url: \$yiiform.attr('action'),
            data: \$yiiform.serializeArray()
        }
    )
    .done(function(data) {
       if(data.success) {
         $('.menu-main-tree').load('/ems-conf/api/get-menu-tree');
        alert('Пункт успешно обновлен');
        } else {
          $('.menu-main-tree').load('/ems-conf/api/get-menu-tree');
          alert('Ошибка при обновлении данных');
        }
    })
    .fail(function () {
      $('.menu-main-tree').load('/ems-conf/api/get-menu-tree');
          alert('Ошибка при добавлении данных');
    })

    return false; // отменяем отправку данных формы
})

SCRIPT;



$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format_icon, View::POS_HEAD);


$icons__b = Yii::$app->params['fa-icons'];
$icons = [];
foreach ($icons__b as $ii){
    $icons[$ii] =$ii;
}
$form = ActiveForm::begin([
    'id' => 'new-menu-item'.$random_id,
    //   'action' => 'save-url',
    'enableAjaxValidation' => true,
    'options' => ['class' => 'form-horizontal form-element'],
    'fieldConfig' => [
        'template' => "<div class='form-group row'>{label}\n<div class='col-sm-10'> {beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}</div></div>",
        'labelOptions'=>['class' => 'col-sm-2 control-label'],
    ],

    //  'validationUrl' => 'my-validation-url',
]); ?>
    <div class="box-body">
        <?= $form->field($model, 'name')->textInput(); ?>
        <?= $form->field($model, 'route')->textInput(); ?>

        <?=       $form->field($model, 'icon')->widget(Select2::classname(), [
            'data' => $icons,
            'options' => ['placeholder' => 'Выберите иконку'],
            'pluginOptions' => [
                'templateResult' => new JsExpression('format_icon'.$random_id),
                'templateSelection' => new JsExpression('format_icon'.$random_id),
                'escapeMarkup' => $escape,
                'allowClear' => true
            ],
        ]); ?>
        <?=       $form->field($model, 'parent')->widget(Select2::classname(), [
            'data' => $s_parents,
            'options' => ['placeholder' => 'Выберите родительский элемент'],
            'pluginOptions' => [

                'escapeMarkup' => $escape,
                'allowClear' => true
            ],
        ]); ?>

        <?= Html::submitButton('Обновить элемент',[    'class' => 'btn btn-success btn-lg']); ?>
    </div>
<?php $form->end(); ?>